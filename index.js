const SerialPort = require('serialport');
const express = require('express');
const app = express();

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile('./public/index.html');
});

// const LED = new SerialPort('/dev/cu.wchusbserial14130', {
//   baudRate: 9600
// });

function map (value, fromMin, fromMax, toMin, toMax) {
    let result = 0;

    result = (value <= fromMin)
      ? toMin : (value >= fromMax)
        ? toMax: (() => {
        let ratio = (toMax - toMin) / (fromMax - fromMin);
        return (value - fromMin) * ratio + toMin;
      })();

    return result;
  }

app.get("/api/score", (req, res, next) => {
  // console.log('score:'+ player1 + ', ' + player2);
  console.log(req.query);
  let header = new Buffer.from('X');
  let p1ledLevel = map(Number(req.query.player1), 0, 80, 0, 10);
  let p2ledLevel = map(Number(req.query.player2), 0, 80, 0, 10);
  let score = new Buffer.from([Math.floor(p1ledLevel), Math.floor(p2ledLevel)]);
  let wait = new Buffer.from([0]);
  let match = new Buffer.from([0]);
  LED.write(Buffer.concat([header, score, wait, match]), (err) => {
    if (err) console.log('Error on write', err.message);
  } );
});

app.get("/api/wait", (req, res, next) => {
  console.log('wait');
  let header = new Buffer.from('X');
  let score = new Buffer.from([0, 0]);
  let wait = new Buffer.from([1]);
  let match = new Buffer.from([0]);
  LED.write(Buffer.concat([header, score, wait, match]), (err) => {
    if (err) console.log('Error on write', err.message);
  } );
});

app.get("/api/match", (req, res, next) => {
  console.log('match');
  let header = new Buffer.from('X');
  let score = new Buffer.from([0, 0]);
  let wait = new Buffer.from([0]);
  let match = new Buffer.from([1]);
  LED.write(Buffer.concat([header, score, wait, match]), (err) => {
    if (err) console.log('Error on write', err.message);
  } );
});

app.get("/api/turnoff", (req, res, next) => {
  console.log('turnoff');
  let header = new Buffer.from('X');
  let score = new Buffer.from([0, 0]);
  let wait = new Buffer.from([0]);
  let match = new Buffer.from([0]);
  LED.write(Buffer.concat([header, score, wait, match]), (err) => {
    if (err) console.log('Error on write', err.message);
  } );
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000');
});
